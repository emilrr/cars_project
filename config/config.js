export default {
  baseURl: process.env.BASE_URL || '/api/v1',
  port: process.env.PORT || 3000,
  db: {
    url: process.env.MONGODB_URL || 'mongodb://admin:pass@ds229008.mlab.com:29008/cars',
    port: process.env.MONGODB_PORT || 27017,
    dbName: process.env.MONGODB_DB_NAME || 'cars'
  },
  bodyParser: {
    enableTypes: ['json'],
    jsonLimit: '5mb',
    strict: true
  }
}