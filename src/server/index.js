import http from 'http'
import config from '../../config/config'
import app from './app'

const runServer = async () => {
  try {
    const server = http.createServer(app.callback()).listen(config.port)

    server.on('close', () => {
      console.log('API server is closed')
    })

    console.log(`API server is started on ${config.port}`)

    return server
  } catch (err) {
    console.log(`Can not start the server: ${err}`)
    return err
  }
}

export default runServer()