import mongoose from 'mongoose'
import config from '../../../config/config'

const connect = () => {
  mongoose.connect(config.db.url)

  let connection = mongoose.connection

  connection.on('connected', () => {
    console.log('Connected to db cars')
  });

  connection.on('disconnected', () => {
    console.log('Disconnected to db cars')
  })

  connection.on('error', (error) => {
    console.log('db connection error', error)
  })

  process.on('SIGINT', () => {
    connection.close(() => {
      console.log('DB connection closed due to process termination')
      process.exit(0)
    })
  })
}

export default connect