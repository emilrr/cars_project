import Koa from 'koa'
import bodyParser from 'koa-bodyparser'
import config from '../../config/config'
import Logger from 'koa-logger'

import connection from '../server/db/mongoConnection'

const options = config.bodyParser

const app = new Koa()

app.use(bodyParser(options))
app.use(Logger())

connection()

app.on('error', (err, ctx) => {
  console.log(err.message)
})

export default app
